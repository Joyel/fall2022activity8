package arraylist;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args){
        String[] initialWords = {"Car", "CAR"};

        System.out.println(ListPractice.countUpperCase(initialWords));

        String[] testThis = ListPractice.getUpperCase(initialWords);

        System.out.println(testThis.length);

        List<String> words = new ArrayList<String>();
        System.out.println(words.size());

        List<String> stillEmpty = new ArrayList<String>(50);
        System.out.println(stillEmpty.size());

        for (String word:initialWords){
            words.add(word);
        }

        System.out.println(words.size());

        System.out.println(words.contains("Car"));

        System.out.println(words.contains("CaR"));

        List<Point> points = new ArrayList<Point>();

        Point point1 = new Point(2, 4);
        Point point2 = new Point(0.2, 1.0);
        Point point3 = new Point(0.4, 0.8);
        points.add(point1);
        points.add(point2);
        points.add(point3);

        Point target = new Point(2, 4);

        System.out.println(points.contains(target));
    }
}
