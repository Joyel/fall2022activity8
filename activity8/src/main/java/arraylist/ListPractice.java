package arraylist;

import java.util.ArrayList;
import java.util.List;

public class ListPractice
{
    public static int countUpperCase(String[] strings){
        int count = 0;
        
        for (int i = 0; i < strings.length; i++){
            if (strings[i].matches("[A-Z]*")){
                count++;
            }
        }

        return count;
    }

    public static String[] getUpperCase(String[] strings){
        int count = 0;
        
        for (int i = 0; i < strings.length; i++){
            if (strings[i].matches("[A-Z]*")){
                count++;
            }
        }

        String[] stringSet = new String[count];

        int populate = 0;

        for (int i = 0; i < strings.length; i++){
            if (strings[i].matches("[A-Z]*")){
                stringSet[populate] = strings[i];
                populate++;
            }
        }

        return stringSet;
    }

    public static int countUpperCase(List<String> words){
        int count = 0;

        for (int i = 0; i < words.size(); i++){
            if (words.get(i).matches("[A-Z]*")){
                count++;
            }
        }
        return count;
    }

    public static List<String> getUpperCase(List<String> words){
        List<String> newWordSet = new ArrayList<String>();

        for (int i = 0; i < words.size(); i++){
            if (words.get(i).matches("[A-Z]*")){
                newWordSet.add(words.get(i));
            }
        }

        return newWordSet;
    }
}
