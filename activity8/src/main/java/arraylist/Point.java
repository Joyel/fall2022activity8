package arraylist;

public class Point {
    public double x;
    public double y;

    public Point (double newX, double newY){
        this.x = newX;
        this.y = newY;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Point)){
            return false;
        }

            Point actuallyAPoint = (Point) other;

        if (this.x == actuallyAPoint.x && this.y == actuallyAPoint.y){
            return true;
        }

        // or return this.x == actuallyAPoint.x && this.y == actuallyAPoint.y;

        return false;
    }
}
